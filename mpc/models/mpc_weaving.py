# -*- coding: utf-8 -*-
# Part of Odoo, odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class mpc_weaving(models.Model):
    """Manufacturing Production Control"""
    _name = 'mpc.weaving'

    def get_weaving_no(self):
        nama_baru = self.env['ir.sequence'].next_by_code('mpc.weaving.no')
        return nama_baru

    name = fields.Char('Id Potong', required=True, index=True, copy=False, default=get_weaving_no)
    no_kp = fields.Many2one('mrp.production', string='MO/KP')
    product_id = fields.Many2one(related='no_kp.product_tmpl_id',string='Nama Barang',store=True,readonly=True,)
    potongke = fields.Char(string='No Potong', )
    tgl_potong = fields.Date(string='Tgl Potong', default=fields.Date.context_today, )
    no_mesin = fields.Many2one('mesin.produksi', string='No Mesin', )
    shift = fields.Char(string='Shift', )
    pjg_potong = fields.Integer(string='Panjang Potong', )
    brt_potong = fields.Integer(string='Berat Potong', default=1)
    notes_potong = fields.Text(string='Notes')
