from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class mpcstockquant(models.Model):
    _inherit = 'stock.quant'

    nw_done = fields.Float('NW')