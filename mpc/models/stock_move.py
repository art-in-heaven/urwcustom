from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

class mpcstockmoveline(models.Model):
    _inherit = 'stock.move.line'

    nw_done = fields.Float('NW')
    no_ins_done = fields.Many2one('mpc.inspek', 'No Inspek')
    tot_po_done = fields.Float('Total Point')
    grade_id_done = fields.Char('Grade')

    @api.multi
    def write(self, vals):
        for move_line in self:
            if move_line.move_id.production_id and 'lot_id' in vals:
                move_line.production_id.move_raw_ids.mapped('move_line_ids')\
                    .filtered(lambda r: r.done_wo and not r.done_move and r.lot_produced_id == move_line.lot_id)\
                    .write({'lot_produced_id': vals['lot_id']})
            production = move_line.move_id.production_id or move_line.move_id.raw_material_production_id
            if production and move_line.state == 'done' and any(field in vals for field in ('lot_id', 'location_id', 'qty_done', 'nw_done', 'no_ins_done', 'tot_po_done', 'grade_id_done')):
                move_line._log_message(production, move_line, 'mrp.track_production_move_template', vals)
        return super(mpcstockmoveline, self).write(vals)