from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError
from odoo.tools import float_is_zero
import math

class mpc_inspek(models.Model):
    _name = 'mpc.inspek'

    def get_inspek_no(self):
        nama_baru = self.env['ir.sequence'].next_by_code('mpc.inspek.no')
        return nama_baru
        
    name = fields.Char('Id Inspek', required=True, index=True, copy=False, default=get_inspek_no)
    no_potong = fields.Many2one('mpc.weaving', string='No Potong', )
    shift_inspek = fields.Char(string='Shift Inspek', )
    jumlah_inspek = fields.Integer(string='Jumlah Inspek', )
    inspek = fields.Char(string='Inspektor',)
    p1 = fields.Integer(string='Point 1',)
    p2 = fields.Integer(string='Point 2',)
    p3 = fields.Integer(string='Point 3',)
    p4 = fields.Integer(string='Point 4',)
    pd = fields.Integer(string='PD',)
    total_point = fields.Float(string='Total Point',compute="_total_semua_point",readonly=True ,store=True,)
    ket_grade = fields.Text(string='Keterangan Grade',)
    tgl_dari = fields.Date(string='Tgl Dari',default=fields.Date.context_today,)
    tgl_sampai = fields.Date(string='Tgl Sampai',default=fields.Date.context_today,)


  
    @api.depends('p1','p2','p3','p4',)
    def _total_semua_point(self):
        #import pdb; pdb.set_trace()
        for r in self:
            if r.p1 and r.p2 and r.p3 and r.p4 != 0.0:
                r.total_point = (r.p1 + r.p2 + r.p3 + r.p4)
            else :
                r.total_point = 0.0