from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

    
class mpcstockproductionlot(models.Model):
    _inherit = 'stock.production.lot'

    nw_done = fields.Float('NW')
    no_ins_done = fields.Many2one('mpc.inspek', 'No Inspek')
    tot_po_done = fields.Float('Total Point')
    grade_id_done = fields.Char('Grade')

    @api.multi
    def write(self, vals):
        if 'product_id' in vals:
            move_lines = self.env['stock.move.line'].search([('lot_id', 'in', self.ids)])
            if move_lines:
                raise UserError(_(
                    'You are not allowed to change the product linked to a serial or lot number ' +
                    'if some stock moves have already been created with that number. ' +
                    'This would lead to inconsistencies in your stock.'
                ))
        return super(mpcstockproductionlot, self).write(vals)