# Copyright (C) 2018 - TODAY, Open Source Integrators
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Manufacturing Production Control",
    "summary": "Penerimaan Produksi dan Inspekting",
    "version": "",
    "license": "AGPL-3",
    "author": "Odoo Community Association (OCA)",
    "category": "MPC",
    "website": "https://gitlab.com/bee-coded/urwcustom",
    "depends": ["mrp", "base", "stock", "product"],
    "data": [
        "views/menu_mpc.xml",
        "views/mpc_inspek.xml",
        "views/mpc_weaving.xml",
        "views/ir_sequence.xml",
        #"views/mrp_production.xml",
        "wizard/mpc_produce.xml",
    ],
    "auto_install": False,
    "installable": True,
    "application": True,
}