# -*- coding: utf-8 -*-
# Part of Odoo, odoo. See LICENSE file for full copyright and licensing details.

from collections import Counter
from datetime import datetime

from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_compare, float_round

class MrpProductProduce(models.TransientModel):
    _inherit = "mrp.product.produce"



    net_weight = fields.Float(string='NW')
    no_inspek = fields.Many2one ('mpc.inspek', 'No Inspek')
    total_point = fields.Float(related="no_inspek.total_point", store=True , string='Total Point')
    grade_id = fields.Selection(string="Grade", compute= '_compute_grade',
                                selection=[('A','A'),
                                 ('B','B'),
                                 ('C','C'),
                                 ('BS','BS'),
                                 ('RK','RK')],copy= False,readonly =True, store=True,)

    @api.depends("total_point","product_qty",)
    def _compute_grade(self):
       #import pdb; pdb.set_trace()
        for grade in self:
           if not grade.total_point/grade.product_qty  >= 0.4:
                grade.grade_id = 'A'
           elif grade.total_point/grade.product_qty <= 1.0:
                grade.grade_id = 'B'
           elif grade.total_point/grade.product_qty <= 1.8:
               grade.grade_id = 'C'
           elif grade.total_point/grade.product_qty <= 2:
                grade.grade_id = "RK"
           elif grade.total_point/grade.product_qty <= 2.5:
                grade.grade_id = 'BS'
           elif grade.total_point/grade.product_qty >= 2.6:  
                grade.grade_id = "BS"
   
    @api.multi
    def do_produce(self):
        # Nothing to do for lots since values are created using default data (stock.move.lots)
        quantity = self.product_qty
        if float_compare(quantity, 0, precision_rounding=self.product_uom_id.rounding) <= 0:
            raise UserError(_("The production order for '%s' has no quantity specified") % self.product_id.display_name)
        for move in self.production_id.move_raw_ids:
            # TODO currently not possible to guess if the user updated quantity by hand or automatically by the produce wizard.
            if move.product_id.tracking == 'none' and move.state not in ('done', 'cancel') and move.unit_factor:
                rounding = move.product_uom.rounding
                if self.product_id.tracking != 'none':
                    qty_to_add = float_round(quantity * move.unit_factor, precision_rounding=rounding)
                    move._generate_consumed_move_line(qty_to_add, self.lot_id)
                else:
                    move.quantity_done += float_round(quantity * move.unit_factor, precision_rounding=rounding)
        for move in self.production_id.move_finished_ids:
            if move.product_id.tracking == 'none' and move.state not in ('done', 'cancel'):
                rounding = move.product_uom.rounding
                if move.product_id.id == self.production_id.product_id.id:
                    move.quantity_done += float_round(quantity, precision_rounding=rounding)
                elif move.unit_factor:
                    # byproducts handling
                    move.quantity_done += float_round(quantity * move.unit_factor, precision_rounding=rounding)
        self.check_finished_move_lots()
        self.write_move_lots()
        if self.production_id.state == 'confirmed':
            self.production_id.write({
                'state': 'progress',
                'date_start': datetime.now(),
            })
        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def check_finished_move_lots(self):
        produce_move = self.production_id.move_finished_ids.filtered(lambda x: x.product_id == self.product_id and x.state not in ('done', 'cancel'))
        if produce_move and produce_move.product_id.tracking != 'none':
            if not self.lot_id:
                raise UserError(_('You need to provide a lot for the finished product'))
            existing_move_line = produce_move.move_line_ids.filtered(lambda x: x.lot_id == self.lot_id)
            if existing_move_line:
                if self.product_id.tracking == 'serial':
                    raise UserError(_('You cannot produce the same serial number twice.'))
                existing_move_line.product_uom_qty += self.product_qty
                existing_move_line.qty_done += self.product_qty
            else:
                vals = {
                'move_id': produce_move.id,
                'product_id': produce_move.product_id.id,
                'production_id': self.production_id.id,
                'product_uom_qty': self.product_qty,
                'product_uom_id': produce_move.product_uom.id,
                'qty_done': self.product_qty,
                'lot_id': self.lot_id.id,
                'location_id': produce_move.location_id.id,
                'location_dest_id': produce_move.location_dest_id.id,
                'nw_done' : self.net_weight,
                'no_ins_done' : self.no_inspek.id,
                'tot_po_done' : self.total_point,
                'grade_id_done' :  self.grade_id,
                }
                self.env['stock.move.line'].create(vals) 

        for pl in self.produce_line_ids:
            if pl.qty_done:
                if not pl.lot_id:
                    raise UserError(_('Please enter a lot or serial number for %s !' % pl.product_id.name))
                if not pl.move_id:
                    # Find move_id that would match
                    move_id = self.production_id.move_raw_ids.filtered(lambda x: x.product_id == pl.product_id and x.state not in ('done', 'cancel'))
                    if move_id:
                        pl.move_id = move_id
                    else:
                        # create a move and put it in there
                        order = self.production_id
                        pl.move_id = self.env['stock.move'].create({
                                    'name': order.name,
                                    'product_id': pl.product_id.id,
                                    'product_uom': pl.product_uom_id.id,
                                    'location_id': order.location_src_id.id,
                                    'location_dest_id': self.product_id.property_stock_production.id,
                                    'raw_material_production_id': order.id,
                                    'group_id': order.procurement_group_id.id,
                                    'origin': order.name,
                                    'state': 'confirmed'})
                pl.move_id._generate_consumed_move_line(pl.qty_done, self.lot_id, lot=pl.lot_id)
        return True

    @api.multi
    def write_move_lots(self):
        if 'product_id' in self:
            move_lines = self.env['stock.move.line'].search([('lot_id', 'in', self.ids)])
            if move_lines:
                raise UserError(_(
                    'You are not allowed to change the product linked to a serial or lot number ' +
                    'if some stock moves have already been created with that number. ' +
                    'This would lead to inconsistencies in your stock.'
                ))
            vals = {
            'nw_done' : self.net_weight,
            'no_ins_done' : self.no_inspek.id,
            'tot_po_done' : self.total_point,
            'grade_id_done' :  self.grade_id,
            }
            self.env['stock.production.lot'].write(vals)