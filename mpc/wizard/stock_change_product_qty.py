from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError


class mpcstockinventory(models.Model):
    _inherit = 'stock.change.product.qty'

    new_nw = fields.Float('NW')